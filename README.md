# Beancount Bundle

(a.k.a. Beancount VS Code)

!!! Split
    This repo is largely concerned with using VS Code as an editor and IDE for Beancount. It should be renamed accordingly. Also, some of the files here, mostly docs, deal with using Beancount more generally. These should be refactored out into a new 'beancount-guide' (or something similar).

The [**Beancount Bundle**](https://gitlab.com/alex_ford/beancount-bundle) is the entry point to the ARF Beancount Suite of projects. These are isolated projects designed to address a particular capability or group of features which are added to the Beancount Core software.

Python 3 is the main programming language used in all Beancount projects. Python Virtual Environments are highly recommended and will be used throughout.

**Table of Contents**

- [Beancount Bundle](#beancount-bundle)
  - [ARF Beancount Suite](#arf-beancount-suite)
  - [Building Beancount from source](#building-beancount-from-source)
  - [Install Beancount from Public Repositories](#install-beancount-from-public-repositories)
  - [Clone the ARF Beancount Suite projects](#clone-the-arf-beancount-suite-projects)
  - [Create/clone your Beancount Data directory](#createclone-your-beancount-data-directory)
  - [Installing Beancount](#installing-beancount)
  - [Installing Visual Studio Code](#installing-visual-studio-code)

## ARF Beancount Suite

| Project Name                                                                | Description                                                                                                                                                                         | Usable as of...                    |
| --------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------- |
| [Beancount Secure](https://gitlab.com/alex_ford/beancount-secure)           | Some ideas which increase the security capabilities of Beancount (which are basically non-existent).                                                                                | Not yet – still in ideation phase. |
| [Beancount INS](https://gitlab.com/alex_ford/beancount-ins)                 | Import and Sync capabilities that I’ve attempted to add to Beancount.                                                                                                               | Yes – prototype phase.             |
| [Beancount Templates](https://gitlab.com/alex_ford/beancount-templates)     | Templates of transactions and entry helpers to make entering these templates easier.                                                                                                | Yes – prototype phase.             |
| [Beancount Vagrant](https://gitlab.com/alex_ford/beancount-vagrant)         | Creates a full Ubuntu based VM with all the necessary software to run Beancount (and Fava) using Vagrant and VirtualBox. (Recommend Beancount Fava Docker instead – see below!)     | Yes.                               |
| [Beancount Scheduler](https://gitlab.com/alex_ford/beancount-scheduler)     | Adds scheduled transactions and related capabilities to Beancount.                                                                                                                  | Yes – prototype phase.             |
| [Beancount Fava](https://gitlab.com/alex_ford/beancount-fava)               |                                                                                                                                                                                     |                                    |
| [Beancount Fava Docker](https://gitlab.com/alex_ford/beancount-fava-docker) | Containerizes Beancount and Fava using Docker technology (thanks to ‘yegle/fava’) and provides useful scripts to interface with Beancount when it is running in a Docker container. | Yes.                               |
| [Beancount Ionic](https://gitlab.com/alex_ford/beancount-ionic)             | A mobile/progressive app for Beancount which uses the Ionic Framework. Can run on basically all devices ranging from desktops, to phones!                                           | Not yet – prototype phase.         |
| [Beancount CLI](https://gitlab.com/alex_ford/beancount-cli)                 |                                                                                                                                                                                     | Not yet.                           |
| [Beancount Scripts](https://gitlab.com/alex_ford/beancount-scripts)         |                                                                                                                                                                                     | Not yet.                           |
| [Beancount Mobileterm](https://gitlab.com/alex_ford/beancount-mobileterm)   |                                                                                                                                                                                     | Not yet.                           |
| [Beancount Filer](https://gitlab.com/alex_ford/beancount-filer)             |                                                                                                                                                                                     | Not yet.                           |

## Building Beancount from source

Coming soon...

## Install Beancount from Public Repositories

A script is available under `./scripts/install-beancount-and-fava.sh` if you need to build Beancount from it's source code.

## Clone the ARF Beancount Suite projects

1.  Create a parent folder to hold all the individual projects, called `./beancount`
2.  Clone this (`beancount-bundle`) mini-project into it, so it resides at `./beancount/beancount-bundle`
    ```bash
    # in ./beancount/
    git clone .../beancount-bundle.git
    ```
3.  Execute the `clone-all-other-repos.sh` (or `.ps1`) script to get the other mini-projects
    ```bash
    cd beancount-bundle
    # now in ./beancount/beancount-bundle/
    ./clone-all-other-repos.sh
    ```

## Create/clone your Beancount Data directory

You will need your very own `./beancount/beancount-data` directory. At some point, there will be a script to help create this, but for now, just make a new directory and follow these conventions:

```filesystem
./beancount/beancount-data
./beancount/beancount-data/accounts/
./beancount/beancount-data/accounts/accounts.beancount
./beancount/beancount-data/accounts/equity.beancount
./beancount/beancount-data/accounts/expenses.beancount
./beancount/beancount-data/accounts/income.beancount
./beancount/beancount-data/accounts/liabilities.beancount
./beancount/beancount-data/configuration/
./beancount/beancount-data/configuration/api
./beancount/beancount-data/configuration/api/(API PRIVATE KEYS HERE)
./beancount/beancount-data/configuration/commodities.beancount
./beancount/beancount-data/configuration/fava-budget.beancount
./beancount/beancount-data/configuration/fava-options.beancount
./beancount/beancount-data/configuration/fava-queries.beancount
./beancount/beancount-data/configuration/fava-sidebar.beancount
./beancount/beancount-data/configuration/ins-payee-rules.bcspec
./beancount/beancount-data/documents/
./beancount/beancount-data/documents/(YEAR)/
./beancount/beancount-data/documents/(YEAR)/(YEAR)-(MONTH)/
./beancount/beancount-data/import-files/
./beancount/beancount-data/import-files/inbox/
./beancount/beancount-data/import-files/processed/
./beancount/beancount-data/notes/
./beancount/beancount-data/other-data/
./beancount/beancount-data/other-data/events.beancount
./beancount/beancount-data/other-data/links.beancount
./beancount/beancount-data/other-data/metadata-fields.beancount
./beancount/beancount-data/other-data/payees.beancount
./beancount/beancount-data/other-data/prices.beancount
./beancount/beancount-data/other-data/sales-taxes.beancount
./beancount/beancount-data/other-data/tags.beancount
./beancount/beancount-data/queries-reports/
./beancount/beancount-data/receipts/
./beancount/beancount-data/receipts/(YEAR)/
./beancount/beancount-data/receipts/(YEAR)/(YEAR)-(MONTH)/
./beancount/beancount-data/scripts/
./beancount/beancount-data/templates/
./beancount/beancount-data/time-tracking/
./beancount/beancount-data/transactions/
./beancount/beancount-data/transactions/(YEAR)/
./beancount/beancount-data/transactions/(YEAR)/
./beancount/beancount-data/transactions/(YEAR)/(YEAR)-(MONTH).beancount
./beancount/beancount-data/websites/
./beancount/beancount-data/websites/alexrichardford.sh
./beancount/beancount-data/websites/beancount-homepage.sh
./beancount/beancount-data/websites/beancount-documentation.sh
./beancount/beancount-data/.gitattributes
./beancount/beancount-data/.gitignore
./beancount/beancount-data/main.beancount
./beancount/beancount-data/PRIVATE_REPO
./beancount/beancount-data/README.md
```

Most of those files should be self-explanitory. Here is a description for a couple of the most improtant files:

-   The `main.beancount` file is where you want to tie all of your other `*.beancount` files together. Here's an example:

```beancount
; Main Beancount Data File
; This ties together all other files using `include` directives.
; In VSCode Beancount extension, don't forget to set the workspace
; setting `beancount.mainBeanFile` to point to this file.

; Options
option "operating_currency" "USD"
option "booking_method" "FIFO"

; include all accounts, commodities, etc.
include "accounts.beancount"
include "commodities.beancount"

; include events
include "events.beancount"

; include prices
include "prices.beancount"

; include fava options and directives
include "fava.beancount"

; include monthly ledgers here
include "AllPriorTransactions.beancount"
include "201806.beancount"
include "201807.beancount"
include "201808.beancount"
include "201809.beancount"

; don't forget to add each new month to the end of this list!
```

## Installing Beancount

1.  Clone the official GitHub mirror: `git clone https://github.com/beancount/beancount`
2.  Install `python3` and `pip3`, if you don't already have them
    -   Windows:
    ```powershell
    TBD
    ```
    -   Linux:
    ```bash
    # only do this if you don't already have python3 and pip3!
    sudo apt-get update
    sudo apt-get install -y python3 python3-pip
    ```
3.  Install `pipenv`
    -   Windows:
    ```powershell
    pip install pipenv
    ```
    -   Linux:
    ```bash
    python3.6 -m pip install pipenv
    ```
4.  Create virtual environment in the parent `beancount` directory
    -   Windows & Linux:
    ```bash
    python3.6 -m pipenv --python 3.6
    ```
5.  Activate the shell for the virtual environment
    ```bash
    # in ./beancount/
    python3.6 -m pipenv shell
    ```
6.  Now build beancount from source:
    ```powershell
    pip3 install python-dateutil bottle ply lxml python-magic beautifulsoup4
    python setup.py install
    ```
    ```bash
    # within (venv)
    pip install python-dateutil bottle ply lxml python-magic beautifulsoup
    sudo python3 setup.py install
    ```
7.  Check install by running one of the Beancount tools:
    1.  Windows
        ```powershell
        bean-file.exe -h
        ```
    2.  Linux
        ```bash
        bean-file -h
        ```
8.  REMEMBER: you'll need to be in the virtual environment to use these tools, OR you can modify your system path to include where they are located.
9.  Install `fava` from source:
    1.  Windows
        1.  <http://gnuwin32.sourceforge.net/packages/make.htm>
            1.  Install it!
            2.  Then run `& "C:\Program Files (x86)\GnuWin32\bin\make.exe"`
        2.  <https://nodejs.org/en/>
            1.  Install it!
        3.  Now use `pip` to install it:
            1.  `pip3 install --editable .`
    2.  Linux
    ```bash
    # within (venv)
    git clone https://github.com/beancount/fava.git
    cd fava
    make
    pip install --editable .
    ```
10. Start `fava` server and open in browser
    ```bash
    # within (venv)
    fava beancount-data/main.beancount &
    google-chrome http://localhost:5000 &
    ```

## Installing Visual Studio Code

1.  Install from website: <https://code.visualstudio.com/>

```bash
wget https://vscode-update.azurewebsites.net/1.26.0/linux-deb-x64/stable
sudo dpkg -i vscode.dpkg
```

1.  Install the following VS Code Extensions, all of which should be recommended automatically by VS Code if you use the provided code-workspace file.
    1.  `Markdown All in One`
    2.  `Beancount`
    3.  `Python`
    4.  `Badgen` (Optional)
    5.  `Badges` (Optional)
    6.  `Excel to Markdown table` (Optional)
    7.  `Markdown Emoji` (Optional)
    8.  `Markdown Extended` (Optional)
    9.  `Markdown Navigation` (Optional)
    10. `Markdown Script` (Optional)
    11. `Markdown+Math` (Optional)
    12. `markdownlint`
    13. `Remark`
    14. `wikilink4md`
2.  Configure the `Beancount` extension.
3.  Load snippets from `beancount-snippets` mini-project.
4.  Load `git-sync-command` for easy access to sync your `beancount-data` repo.
