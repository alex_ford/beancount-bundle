/**
 * VS Code Workspace settings with recommended starting values which should be
 * suitable for most user's when first getting started with using VS Code as
 * their Beancount IDE.
 *
 * Feel free to change this as desired!
 *
 * Author: Alex Richard Ford (arf4188@gmail.com)
 **/
{
  "folders": [
    {
      "path": "."
    },
    {
      "path": "../../../../Data/Personal/beancount-data",
      "name": "beancount-data (private)"
    },
    {
      "path": "../beancount-scripts"
    },
    {
      "path": "../beancount-arfsrc"
    },
    {
      "path": "../beancount-fava-docker"
    },
    {
      "path": "../beancount-ins"
    },
    {
      "path": "../beancount-ionic"
    },
    {
      "path": "../beancount-scheduler"
    },
    {
      "path": "../beancount-secure"
    },
    {
      "path": "../beancount-templates"
    },
    {
      "path": "../beancount-cli"
    },
    {
      "path": "../../arflib"
    }
  ],
  "launch": {
    "configurations": [],
    "compounds": []
  },
  "tasks": {
    "version": "2.0.0",
    "tasks": []
  },
  "extensions": {
    "recommendations": [
      // Beancount
      "Lencerf.beancount",
      "dongfg.vscode-beancount-formatter",
      // Financial, Accounting, and Math
      "softwareape.numbermonger",
      // Cryptocurrency
      "udayankumar.cryptoticker",
      // Scripting/Programming Language support
      "ms-vscode.powershell",
      "ms-python.python",
      // Data Generation
      "deerawan.vscode-faker",
      "jrebocho.vscode-random",
      // Git
      // "eamodio.gitlens",
      "codezombiech.gitignore",
      "donjayamanne.githistory",
      // "waderyan.gitblame",
      // Data Formats
      "redhat.vscode-yaml",
      "GrapeCity.gc-excelviewer",
      "mechatroner.rainbow-csv",
      "eriklynd.json-tools",
      "tomoki1207.pdf",
      "Gruntfuggly.todo-tree",
      "esbenp.prettier-vscode",
      "stkb.rewrap",
      "fabiospampinato.vscode-highlight",
      // Color and Icon Themes
      "PKief.material-icon-theme",
      "daylerees.rainglow",
      "christian-kohler.path-intellisense",
      "ryu1kn.partial-diff",
      "dchanco.vsc-invoke",
      "sleistner.vscode-fileutils",
      "sandcastle.vscode-open",
      "techer.open-in-browser",
      // Markdown
      "yzhang.markdown-all-in-one",
      "shd101wyy.markdown-preview-enhanced",
      "jebbs.markdown-extended",
      "mrmlnc.vscode-remark",
      "RomanPeshkov.vscode-text-tables",
      "wraith13.zoombar-vscode",
      "2gua.rainbow-brackets",
      "formulahendry.code-runner",
      "dakara.transformer",
      "auchenberg.vscode-browser-preview",
      "fabiospampinato.vscode-commands",
      "qcz.text-power-tools",
      "silesky.toggle-boolean",
      "coenraads.bracket-pair-colorizer-2",
      "randomfractalsinc.vscode-data-preview",
      "streetsidesoftware.code-spell-checker",
      "wwm.better-align"
    ],
    "unwantedRecommendations": [
      "davidanson.vscode-markdownlint"
    ]
  },
  "settings": {
    /************************************************************************
     * Extension: Beancount settings
     ***********************************************************************/
    // Controls whether the auto completion list should include payee and narration fields.
    "beancount.completePayeeNarration": true,
    // Specify the path of Fava if Fava is not installed in the main Python installation.
    "beancount.favaPath": "fava",
    // Provide a map from flag value (a string) to VS Code severity level, or null to not
    // show a warning for this flag.
    "beancount.flagWarnings": {
      "*": null,
      "!": 1,
      "P": null,
      "S": null,
      "T": null,
      "C": null,
      "U": null,
      "R": null,
      "M": null
    },
    "remote.SSH.enableAgentForwarding": true,
    "remote.SSH.enableDynamicForwarding": true,
    "remote.SSH.defaultForwardedPorts": [
      {
        "localPort": 5000,
        "name": "0.0.0.0",
        "remotePort": 5000
      }
    ],
    // Set to true to align the amount (like 1.00 BTC) once a decimal point is inserted.
    // "False" is preferred since we also have Beancount Formatter installed.
    "beancount.instantAlignment": false,
    // If you are splitting beancount files into multiple files, set this value to either the full path or the relative
    // path to your main bean file so that this extension can get all account information. If it is left blank, the
    // extension will consider the file in the current window as the main file.
    "beancount.mainBeanFile": "../../../../Data/Personal/beancount-data/main.beancount",
    // If it is set to true, fava will run once this extension is activated.
    "beancount.runFavaOnActivate": false, // false, as this is more annoying than not
    // Specify the column of the decimal separator.
    "beancount.separatorColumn": 50, // Doesn't matter with beancountFormatter installed
    // Specify the path of Python if beancount is not installed in the main Python installation.
    // Set this to be where your virtual environment for beancount-bundle is!
    // "beancount.python3Path": "python3", // (default)
    // "beancount.python3Path": "C:/Users/arf41/.virtualenvs/beancount-bundle-wZiBeRd5/Scripts/python",
    //**********************************************************************//
    // Extension: Beancount Formatter
    //**********************************************************************//
    "beancountFormatter.binPath": "bean-format",
    "beancountFormatter.currencyColumn": 65,
    "beancountFormatter.numWidth": null,
    "beancountFormatter.prefixWidth": null,
    /**********************************************************************
     * Extension: Browser Preview
     *
     * This contributes a button/icon on the Activities Bar (to the left) which we've reconfigured to launch
     * Fava right within VS Code. You can change the URL to any page in Fava, as a quick way to get to the most useful
     * page for your workflow!
     ***********************************************************************/
    // "browser-preview.startUrl": "http://localhost:5000", // (default)
    // "browser-preview.startUrl": "http://localhost:5000/beancount/income_statement/?interval=day&time=day-14+to+day%2B14",
    "browser-preview.startUrl": "http://miner01:5000/beancount/income_statement/?interval=day&time=day-14+to+day%2B14",
    // Beancount language specific settings
    "[beancount]": {
      "editor.acceptSuggestionOnEnter": "smart",
      "editor.autoClosingBrackets": "always",
      "editor.autoClosingQuotes": "always",
      "editor.autoIndent": "full",
      "editor.codeLens": true,
      "editor.defaultFormatter": "dongfg.vscode-beancount-formatter",
      // "editor.defaultFormatter": "Lencerf.beancount",
      "editor.rulers": [2, 4, 6, 8, 45, 64, 90],
      "editor.suggest.filterGraceful": true,
      "editor.suggestSelection": "recentlyUsed",
      "editor.tabCompletion": "onlySnippets",
      "editor.quickSuggestions": {
        "other": false,
        "comments": false,
        "strings": false
      },
      "editor.tabSize": 2,
      "editor.trimAutoWhitespace": true,
      "editor.useTabStops": true,
      "editor.wordWrap": "off",
      "files.encoding": "utf8"
    }, // end Beancount language-specific settings
    "[markdown]": {
      "editor.defaultFormatter": "yzhang.markdown-all-in-one"
    },
    "cryptoticker.cryptoCurrency": "USD",
    // "cryptoticker.cryptoExchange": "CCCAGG",
    "cryptoticker.cryptoExchange": "Coinbase",
    "cryptoticker.cryptoSymbols": [
      "BTC",
      "ETH",
      "LTC",
      "XMR"
    ],
    // Default Editor settings (see Beancount Editor settings for those)
    "editor.rulers": [120],
    "editor.acceptSuggestionOnEnter": "on",
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.find.autoFindInSelection": "multiline",
    "editor.links": true,
    "editor.formatOnPaste": true,
    "editor.formatOnSave": false,
    "editor.minimap.enabled": false,
    "breadcrumbs.enabled": true,
    "editor.tabCompletion": "on",
    "editor.tabSize": 4,
    "editor.wordWrap": "off",
    "editor.suggest.snippetsPreventQuickSuggestions": true,
    "explorer.autoReveal": false,
    "files.associations": {
      "*.yaml": "home-assistant"
    },
    "files.autoSave": "onFocusChange",
    "files.defaultLanguage": "beancount",
    "files.enableTrash": true,
    "files.encoding": "utf8",
    "files.eol": "\n",
    "files.insertFinalNewline": true,
    "files.watcherExclude": {
      "**/.git/objects/**": true,
      "**/.git/subtree-cache/**": true,
      "**/node_modules/**": true,
      "**/.vscode/extensions/": true,
      "**/.vscode/extensions/**": true,
      "**/.vscode/user-data/": true,
      "**/.vscode/user-data/**": true,
      "**/.pytest_cache/": true
    },
    // Git settings
    "git.autofetch": true,
    "git.autofetchPeriod": 900,
    "git.autorefresh": true,
    "git.autoStash": false,
    "git.confirmSync": false,
    "git.countBadge": "all",
    "git.decorations.enabled": true,
    "git.detectSubmodules": true,
    "git.enabled": true,
    "git.enableSmartCommit": false,
    "git.fetchOnPull": true,
    "git.inputValidation": false,
    "git.postCommitCommand": "push",
    "git.promptToSaveFilesBeforeCommit": "never",
    "git.showProgress": true,
    "git.showPushSuccessNotification": true,
    "merge-conflict.codeLens.enabled": true,
    "window.closeWhenEmpty": false,
    "window.title": "💲 [Beancount] ${dirty}${activeEditorShort}${separator}${rootName}${separator}",
    // "workbench.colorTheme": "VSC Military Style",
    "workbench.colorTheme": "Glance (rainglow)",
    "workbench.editor.restoreViewState": true,
    "workbench.editor.showIcons": true,
    "workbench.iconTheme": "material-icon-theme",
    "workbench.editor.revealIfOpen": true,
    "workbench.view.alwaysShowHeaderActions": true,
    "telemetry.enableCrashReporter": false,
    "telemetry.enableTelemetry": false,
    "partialDiff.enableTelemetry": false,
    "highlight.decorations": {
      "rangeBehavior": 3
    },
    "highlight.maxMatches": 250,
    "highlight.regexFlags": "gi",
    "highlight.regexes": {
      /**
       * See https://marketplace.visualstudio.com/itemdetails?itemName=fabiospampinato.vscode-highlight
       * Each () is a capture group, and the arrays of formatting/decorations to apply
       * map to each capture group defined.
       * https://code.visualstudio.com/api/references/vscode-api#DecorationRenderOptions
       * http://www.stylinwithcss.com/resources_css_properties.php
       */
      /**
       * Let's highlight the entire line when a beancount transaction has
       * the '!' flag instead of the '*' flag.
       */
      "(([\\d]+|  ))( ! )": {
        "filterLanguagesRegex": "beancount",
        "decorations": [
          {},
          {
            "color": "#000000",
            "backgroundColor": "#FF7777",
            "overviewRulerColor": "#FF7777",
            "isWholeLine": false,
            "textDecoration": "none",
            "fontWeight": "bold"
          }
        ]
      },
      /**
       * For Boolean markers, let's highlight 'false' so it is called out
       * more... this usually means something hasn't been done yet!
       */
      "(estimated: )(TRUE)": {
        "filterLanguageRegex": "beancount",
        "decorations": [
          {
            "backgroundColor": "#663311",
            "isWholeLine": true
          },
          {
            "color": "#FFA500",
            "backgroundColor": "#994400",
            "overviewRulerColor": "#FFA500",
            "isWholeLine": false
          }
        ]
      },
      "(: )(FALSE)": {
        "filterLanguageRegex": "beancount",
        "decorations": [
          {},
          {
            "color": "#FF0000",
            "backgroundColor": "#000000",
            "overviewRulerColor": "#FF0000",
            "isWholeLine": false
          }
        ]
      },
      /**
       * Highlight a line yellow if we see the word "TODO:".
       */
      "(TODO:)": {
        "filterLanguageRegex": "beancount",
        "decorations": [
          {
            "color": "#000000",
            "backgroundColor": "#FFFF00",
            "overviewRulerColor": "#FFFF00",
            "isWholeLine": false
          }
        ]
      },
      /**
       * Color "payee" differently in order to help distinguish the
       * different parts of a Beancount directive's first line.
       */
      "([*|!] \")(.*?)(\")": {
        "filterLanguageRegex": "beancount",
        "decorations": [
          {},
          {
            "color": "#FFFFFF",
            "fontWeight": "bolder",
            "isWholeLine": false
          },
          {}
        ]
      }
    },
    //**********************************************************************//
    // Markdown settings
    //**********************************************************************//
    "markdown.extension.tableFormatter.enabled": true,
    "markdown.extension.toc.updateOnSave": true,
    "markdown.extension.list.indentationSize": "inherit",
    "markdown.extension.print.absoluteImgPath": false,
    //**********************************************************************//
    // Extension: Markdown Preview Enhanced settings
    //**********************************************************************//
    "markdown-preview-enhanced.enableEmojiSyntax": true,
    "markdown-preview-enhanced.enableScriptExecution": true,
    "markdown-preview-enhanced.enableExtendedTableSyntax": true,
    "markdown-preview-enhanced.enableTypographer": true,
    //**********************************************************************//
    // Extension: Open in Browser settings
    //**********************************************************************//
    "open-in-browser.default": "google-chrome",
    //**********************************************************************//
    // Extension: Python settings
    //**********************************************************************//
    "python.formatting.provider": "autopep8",
    "python.globalModuleInstallation": false,
    // Python: Unit Testing, only 1 of these should be active at a time
    "python.testing.pytestEnabled": true,
    "python.testing.pytestArgs": ["-s"],
    "python.testing.nosetestsEnabled": false,
    "python.testing.unittestEnabled": false,
    "python.testing.autoTestDiscoverOnSaveEnabled": true,
    // Python linter specific
    "python.linting.enabled": true,
    "python.linting.flake8Enabled": false,
    "python.linting.lintOnSave": true,
    "python.linting.mypyEnabled": false,
    "python.linting.banditEnabled": false,
    "python.linting.pycodestyleEnabled": false,
    "python.linting.prospectorEnabled": false,
    "python.linting.pydocstyleEnabled": false,
    "python.linting.pylamaEnabled": false,
    "python.linting.pylintEnabled": true,
    "python.terminal.activateEnvironment": true,
    //**********************************************************************//
    // Extension: Path Intellisense settings
    //**********************************************************************//
    "path-intellisense.autoSlashAfterDirectory": true,
    //**********************************************************************//
    // Built-in: Terminal settings
    //**********************************************************************//
    "terminal.explorerKind": "external",
    "terminal.integrated.copyOnSelection": true,
    //**********************************************************************//
    // Extension: YAML settings
    //**********************************************************************//
    "yaml.completion": true,
    "yaml.format.enable": true,
    "yaml.hover": true,
    "yaml.validate": true,
    //**********************************************************************//
    // Built-in: Search settings
    //**********************************************************************//
    "search.smartCase": true,
    //**********************************************************************//
    // Extension: Code Spell settings
    //**********************************************************************//
    "cSpell.words": [
      "Beancount",
      "Binance",
      "Fava",
      "GDAX",
      "Monero",
      "Paypal",
      "Venmo",
      "dongfg",
      "pytest",
      "txid"
    ],
    "cSpell.ignoreWords": [],
    "cSpell.allowCompoundWords": true,
    "cSpell.enabled": true,
    "cSpell.language": "en",
    "cSpell.enabledLanguageIds": [
      "asciidoc",
      "beancount",
      "c",
      "cpp",
      "csharp",
      "css",
      "git-commit",
      "go",
      "handlebars",
      "haskell",
      "html",
      "jade",
      "java",
      "javascript",
      "javascriptreact",
      "json",
      "jsonc",
      "latex",
      "less",
      "markdown",
      "php",
      "plaintext",
      "python",
      "pug",
      "restructuredtext",
      "rust",
      "scala",
      "scss",
      "text",
      "typescript",
      "typescriptreact",
      "yaml",
      "yml"
    ],
    "todo-tree.tree.autoRefresh": true,
    "todo-tree.tree.labelFormat": "${tag} ${after}",
    "todo-tree.regex.regex": "((//|#|<!--|;|/\\*|^)\\s*($TAGS)|^\\s*- \\[ \\])",
    "todo-tree.general.statusBar": "top three",
    "todo-tree.general.tags": ["TODO", "todo", "FIXME"],
    "todo-tree.tree.trackFile": true,
    // "workbench.colorCustomizations": {
    //   "terminal.background": "#002211",
    //   "editor.background": "#003322",
    //   "editor.hoverHighlightBackground": "#14ad1457",
    //   "editor.lineHighlightBackground": "#69696970",
    //   "titleBar.activeBackground": "#005544",
    //   "titleBar.border": "#333333",
    //   "statusBarItem.remoteBackground": "#448800",
    //   "statusBar.border": "#333333",
    //   "statusBar.background": "#005544",
    //   "notificationCenter.border": "#333333",
    //   "notifications.border": "#333333",
    //   "notifications.background": "#228844",
    //   "sideBar.background": "#002211",
    //   "sideBar.border": "#333333",
    //   "sideBarSectionHeader.background": "#334433",
    //   "activityBar.background": "#334433",
    //   "activityBar.border": "#333333",
    //   "tab.border": "#333333",
    //   "diffEditor.border": "#333333",
    //   "dropdown.border": "#333333",
    //   "breadcrumb.background": "#004433",
    //   "editorGroupHeader.tabsBackground": "#002211"
    // }
  }
}
