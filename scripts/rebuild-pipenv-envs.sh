#!/bin/bash

PROJECTS=(
    "../../beancount-arfsrc"
    "../../beancount-bundle"
    "../../beancount-cli"
    "../../beancount-fava-docker"
    "../../beancount-ins"
    "../../beancount-ionic"
    "../../beancount-scheduler"
    "../../beancount-scripts"
    "../../beancount-secure"
    "../../beancount-templates"
)

for P in ${PROJECTS[@]}; do
    echo "${P}"
    cd "${P}"
    if [ -f "Pipfile" ]; then
        pipenv --rm
        pipenv install
    fi
    cd -
    echo "Finished with ${P}"
done
