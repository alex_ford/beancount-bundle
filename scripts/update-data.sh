#!/bin/bash

# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see './LICENSE')

BUNDLE_SCRIPTS_DIR="$(dirname $(realpath ${0}))"
BUNDLE_HOME_DIR="$(dirname ${BUNDLE_SCRIPTS_DIR})"
BEANCOUNT_WORKSPACE_DIR="$(dirname ${BUNDLE_HOME_DIR})"
BEANC_DATA_DIR="${BEANCOUNT_WORKSPACE_DIR}/beancount-data/"
BEANCOUNT_INS_HOME_DIR="$(realpath ${BEANCOUNT_WORKSPACE_DIR}/beancount-ins)"
# TODO: don't access the scripts directly like this, use pipenv run ...
INS_CRYPTOS_SCRIPT="$(realpath ${BEANCOUNT_INS_HOME_DIR}/beancount_ins/update_crypto_prices.py)"
INS_STOCKS_SCRIPT="$(realpath ${BEANCOUNT_INS_HOME_DIR}/beancount_ins/update_stock_prices.py)"

function continuePrompt() {
    # TODO: add color to the following prompt
    read -p "Press any key to continue, or CTRL+C to abort."
}
function _echof() {
    MSG="${1}"
    LEVEL="${2}"
    LEVEL_COLOR="${3}"
    RESET="\e[0m"
    echo -e "[ ${LEVEL_COLOR}${LEVEL}${RESET} ] ${MSG}"
}
function debug() {
    MSG="${1}"
    _echof "${MSG}" "DBUG" "\e[30m"
}
function info() {
    MSG="${1}"
    _echof "${MSG}" "INFO" "\e[32m"
}
function warn() {
    MSG="${1}"
    _echof "${MSG}" "WARN" "\e[33m"
}
function error() {
    MSG="${1}"
    _echof "${MSG}" "ERRO" "\e[31m"
}

ORIGIN_DIR="$(pwd)"

info "initial Git pull-push of data repo..."
debug "BEANC_DATA_DIR is: '${BEANC_DATA_DIR}'"
cd ${BEANC_DATA_DIR} &&
    git pull &&
    git push
cd ${ORIGIN_DIR}

info "Cryptocurrency price updates are next!"
continuePrompt

info "updating cryptocurrency prices..."
PRICES_FILE="${BEANCOUNT_WORKSPACE_DIR}/beancount-data/other-data/prices.beancount"
cd "${BEANCOUNT_INS_HOME_DIR}" &&
    pipenv run python ${INS_CRYPTOS_SCRIPT} \
               --save ${PRICES_FILE}
cd "${ORIGIN_DIR}"

info "Stock price updates are next!"
continuePrompt

info "updating stock prices..."
cd "${BEANCOUNT_INS_HOME_DIR}" &&
    pipenv run python ${INS_STOCKS_SCRIPT} \
               --save ${PRICES_FILE}
cd "${ORIGIN_DIR}"

info "Final Git synchronization is next!"
continuePrompt

info "final Git commit-pull-push of data repo..."
cd ${BEANC_DATA_DIR} &&
    git add --all . &&
    git commit -m \
        "Automatic data updates performed on $(hostname)" &&
    git pull &&
    git push
cd ${ORIGIN_DIR}

info "Beancount price and commodity data has been updated!"

