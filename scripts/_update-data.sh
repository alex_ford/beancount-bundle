#!/bin/bash
# source me!

function bean-update-data() {
    BEANC_SCRIPTS_SCRIPTS_DIR="$(dirname $(realpath ${BASH_SOURCE}))"
    BEANC_SCRIPTS_DIR="$(dirname ${BEANC_SCRIPTS_SCRIPTS_DIR})"
    ORIGIN_DIR="$(pwd)"

    cd ${BEANC_SCRIPTS_DIR} &&
        ./scripts/update-data.sh
    cd ${ORIGIN_DIR}
}
