#!/bin/bash

THIS_SCRIPT="$(realpath $0)"
THIS_DIR="$(dirname ${THIS_SCRIPT})"
PARENT_DIR="$(dirname ${THIS_DIR})"

pushd "${PARENT_DIR}"
code "${PARENT_DIR}/vscode-beancount.code-workspace"
popd

