#!/bin/bash
# source me!

# Provides various "cd-" helper aliases for quickly navigating to important
# directories.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see './LICENSE')

alias cd-beancount="cd ${HOME}/Workspaces-Personal/beancount"
alias cd-beanc="cd-beancount"
alias cd-beancount-data="cd ${HOME}/Workspaces-Personal/beancount/beancount-data"
alias cd-beanc-data="cd-beancount-data"
alias cd-beancount-bundle="cd ${HOME}/Workspaces-Personal/beancount/beancount-bundle"
alias cd-beanc-bundle="cd beanc-bundle"
