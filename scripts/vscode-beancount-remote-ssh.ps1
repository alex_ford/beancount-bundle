<#
    Windows PowerShell version of the script that launches the isolated
    VS Code environment for Beancount, located within a VM running on this
    Windows host. Leverages VS Code's Remote SSH extension!

        powershell.exe -NoLogo -NonInteractive -WindowStyle Hidden C:\Path\To\This\Script.ps1
    
    Actual:
        
        C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -NoLogo -NonInteractive -WindowStyle Hidden C:\Users\arf41\Workspaces-Personal\beancount\beancount-bundle\scripts\vscode-beancount-remote-ssh.ps1
    
    Author: Alex Richard Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License
#>

$SCRIPTS_DIR    = $PSScriptRoot
$PROJECT_DIR    = Split-Path -Parent $SCRIPTS_DIR
$WORKSPACE_FILE = "vscode-beancount.code-workspace"

# $VSCODE = "${env:LOCALAPPDATA}\Programs\Microsoft VS Code\Code.exe"
# if (-Not (Test-Path $VSCODE)) {
#     Write-Error "Visual Studio Code not found at: $VSCODE"
#     exit 1
# }
$VSCODE = "code"

# TODO: determine current data file and pass that to VSCODE so it opens right away

$CURRENT_DIR = Get-Location

Set-Location $PROJECT_DIR

if (-Not (Test-Path $WORKSPACE_FILE)) {
    Write-Error "VS Code workspace file '${WORKSPACE_FILE}' doesn't exist!"
    exit 1
}

# Launch VS Code for Beancount with user-data and extensions-dir set to
# isolation paths
# For jumping right into a VS Code Remote SSH Workspace:
# code --folder-uri "vscode-remote://ssh-remote+remote_server_name/path/as/needed.md"
$REMOTE_HOST = "devel-vm-2"

# & $VSCODE `
#     --verbose `
#     --user-data-dir ".\.vscode\user-data\" `
#     --extensions-dir ".\.vscode\extensions\" `
#     --folder-uri "vscode-remote://ssh-remote+${REMOTE_HOST}/home/vagrant/Workspaces/Personal/beancount/beancount-bundle/"
#     # $WORKSPACE_FILE `
#     # $args

# & ${VSCODE} --folder-uri "vscode-remote://ssh-remote+${REMOTE_HOST}/home/vagrant/Workspaces/Personal/beancount/beancount-bundle/"
& ${VSCODE} --file-uri "vscode-remote://ssh-remote+${REMOTE_HOST}/home/vagrant/Workspaces/Personal/beancount/beancount-bundle/vscode-beancount.code-workspace"

# Return the user's shell back to where they started
Set-Location ${CURRENT_DIR}
