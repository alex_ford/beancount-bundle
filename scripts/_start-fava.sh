#!/bin/bash
# source me!

# This installs functions into your Bash environment which simply let you
# start the Fava server and webapp from anywhere, just by typing 'start-fava'.
#
# Usage: use the provided ./install-bashrc-starter.sh script
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see './LICENSE')

function __start-fava-helper() {
    echo "[ INFO ] Starting Fava web server in the background..."
    nohup pipenv run fava "./beancount-data/main.beancount" &
    echo "[ INFO ] Please wait while Fava warms up!"
    sleep 6
    echo "[ INFO ] Launching Google Chrome directed at 'http://localhost:5000'"
    google-chrome --app=http://localhost:5000
}

function start-fava() {
    SCRIPTS_DIR="$(dirname $(realpath ${BASH_SOURCE}))"
    BUNDLE_DIR="$(dirname ${SCRIPTS_DIR})"
    BEANCOUNT_GROUP_HOME="$(dirname ${BUNDLE_DIR})"
    ORIGIN_DIR="$(pwd)"
    cd "${BEANCOUNT_GROUP_HOME}" &&
        __start-fava-helper
        #stop-fava
    cd "${ORIGIN_DIR}"
}

function status-fava() {
    ps -aef | grep -i fava
}

function stop-fava() {
    echo "[ INFO ] stopping Fava background server..."
    pkill fava
    echo "[ INFO ] all done!"
}
