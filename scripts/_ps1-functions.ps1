$BEANCOUNT_GROUP_DIR    = Resolve-Path "${HOME}\Workspaces-Personal\beancount\"
$BEANCOUNT_BUNDLE_DIR   = Resolve-Path "${BEANCOUNT_GROUP_DIR}\beancount-bundle\"
$BEANCOUNT_DATA_DIR     = Resolve-Path "${BEANCOUNT_GROUP_DIR}\beancount-data\"

<#
    Open Microsoft Visual Studio Code for Beancount.
#>
function bean-vscode() {
    & "${BEANCOUNT_BUNDLE_DIR}\scripts\vscode-beancount.ps1" $args
}
