#!/bin/bash

THIS_SCRIPT="$(realpath $0)"
THIS_SCRIPT_DIR="$(dirname ${THIS_SCRIPT})"
BEANCOUNT_BUNDLE_DIR="$(dirname ${THIS_SCRIPT_DIR})"
BEANCOUNT_GROUP_DIR="$(dirname ${BEANCOUNT_BUNDLE_DIR})"

PROJECTS=(
    "${BEANCOUNT_GROUP_DIR}/beancount-ins"
    "${BEANCOUNT_GROUP_DIR}/beancount-scripts"
    "${BEANCOUNT_GROUP_DIR}/beancount-scheduler"
    "${BEANCOUNT_GROUP_DIR}/beancount-templates"
    "${BEANCOUNT_GROUP_DIR}/beancount-cli"
)

for PROJECT in ${PROJECTS[@]}; do
    echo "Working on => ${PROJECT}"
    pushd "${PROJECT}" || exit
    # pipenv install
    ./scripts/install.sh
    popd || exit
done
