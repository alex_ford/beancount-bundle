#!/bin/bash

# Use this script to launch and/or open files in an isolated, indepedent
# instance of Visual Studio Code.
#
# Usage 1: directly from this script
#       ./vscode-beancount.sh
#   or  ./vscode-beancount.sh file1 file2...
#
# Usage 2: install bin link and run from anywhere!
#       ./install-vscode-beancount.sh
#       vscode-beancount
#   or  vscode-beancount file1 file2...
#
# Author: Alex Richard Ford (arf4188@gmail.com)

function error() {
    MSG="$@"
    echo -e "[ \e[31mERRO\e[0m ] ${MSG}"
    exit 1
}

function warn() {
    MSG="$@"
    echo -e "[ \e[32mWARN\e[0m ] ${MSG}"
}

function info() {
    MSG="$@"
    echo -e "[ \e[34mINFO\e[0m ] ${MSG}"
}

function debug() {
    if [ "${DBUG}" == 1 ]; then
        MSG="$@"
        echo -e "[ \e[33mDBUG\e[0m ] ${MSG}"
    fi
}

# Visual Studio Code must already be installed
CODE_BIN="$(which code)"
if [ -z "${CODE_BIN}" ]; then error "Visual Studio Code could not be resolved. Is it installed?"; fi
debug "CODE_BIN is: ${CODE_BIN}"

# Handles the difference between sourcing this script, vs executing it.
if [ -z ${BASH_SOURCE} ]; then
    BUNDLE_SCRIPTS_DIR="$(dirname $(realpath ${0}))"
else
    BUNDLE_SCRIPTS_DIR="$(dirname $(realpath ${BASH_SOURCE}))"
fi
debug "BUNDLE_SCRIPTS_DIR is: ${BUNDLE_SCRIPTS_DIR}"

# Should resolve to the parent directory of a "scripts" directory.
BUNDLE_HOME_DIR="$(dirname ${BUNDLE_SCRIPTS_DIR})"
debug "BUNDLE_HOME_DIR is: ${BUNDLE_HOME_DIR}"

ORIGIN_DIR="$(pwd)"
debug "ORIGIN_DIR is: ${ORIGIN_DIR}"

cd "${BUNDLE_HOME_DIR}" &&
    info "Launching VSCode for Beancount!" &&
    pipenv run python beancount_bundle/vscode_environment.py $@
cd "${ORIGIN_DIR}"
