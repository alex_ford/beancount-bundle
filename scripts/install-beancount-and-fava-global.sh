#!/bin/bash

###
# Install script for prerequisite software needed to leverage Beancount and the
# associated ecosystem of software on this system.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License
###

# supports Termux, and therefore similar single-mode unix systems (i.e. resolve sudo dynamically)
SUDO="$(which sudo)"
PKGMGR="$(which pkg)"
if [ "${PKGMGR}" == "" ]; then
    PKGMGR="$(which apt)"
fi
if [ "${PKGMGR}" == "" ]; then
    echo "[ ERRO ] no supported package manager found."
    exit 1
fi

# TODO: add logging/output functions with colorized level tags

# TODO: refactor last command error check into a function for better code quality

# TODO: add ubiqutous execution code so this script will work if executed from another directory

echo "[ INFO ] Installing Python 3 and Pip from APT Repository..."
PYTHON="$(which python)"
PIP="$(which pip)"
if [ "${PYTHON}" == "" ]; then
    $SUDO $PKGMGR install -y python3
    if [ $? != 0 ]; then echo "[ ERRO ] there was an error!"; exit 1; fi
fi
if [ "${PIP}" == "" ]; then
    $SUDO $PKGMGR install -y python3-pip
    if [ $? != 0 ]; then echo "[ ERRO ] there was an error!"; exit 1; fi
fi

echo "[ INFO ] Installing Pipenv from PyPi Repository..."
$PIP install pipenv
if [ $? != 0 ]; then echo "[ ERRO ] there was an error!"; exit 1; fi

echo "[ INFO ] Installing Beancount from PyPi Repository..."
python3 -m pip install beancount
if [ $? != 0 ]; then echo "[ ERRO ] there was an error!"; exit 1; fi

echo "[ INFO ] Installing Fava from PyPi Repository..."
#python3 -m pip install --user fava # includes 'beancount' as a dependency
python3 -m pip install fava
if [ $? != 0 ]; then echo "[ ERRO ] there was an error!"; exit 1; fi

# TODO: don't do this if it is unnecessary! Maybe can just check if one of the 'bean' core utils is on the path?
#echo "[ INFO ] Adding Python user bin location to the path..."
#echo "export PATH=/home/alex/.local/bin:${PATH}" >> ~/.bashrc

echo "[ INFO ] Restart bash or 'source ~/.bashrc' to reload PATH environment variable."

