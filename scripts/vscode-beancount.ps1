<#
    Windows PowerShell version of the script that launches the isolated
    VS Code environment for Beancount.

    This script can be used as a direct replacement when calling
    "code" from the command line to open files, diff them, etc.
    
        e.g. vscode-beancount.ps1 --diff f1 f2
    
    The Windows Shortcut should point to something like this:
    
        powershell.exe -NoLogo -NonInteractive -WindowStyle Hidden C:\Path\To\This\Script.ps1
    
    Actual:
        
        C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -NoLogo -NonInteractive -WindowStyle Hidden C:\Users\arf41\Workspaces-Personal\beancount\beancount-bundle\scripts\vscode-beancount.ps1
    
    Author: Alex Richard Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License
#>

$SCRIPTS_DIR    = $PSScriptRoot
$PROJECT_DIR    = Split-Path -Parent $SCRIPTS_DIR
$WORKSPACE_FILE = "vscode-beancount.code-workspace"

# $VSCODE = "${env:LOCALAPPDATA}\Programs\Microsoft VS Code\Code.exe"
$VSCODE = "C:\Program Files\Microsoft VS Code\bin\code.cmd"
if (-Not (Test-Path $VSCODE)) {
    Write-Error "Visual Studio Code not found at: $VSCODE"
    Write-Host -NoNewLine 'Press any key to continue...'
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
    exit 1
}

# TODO: determine current data file and pass that to VSCODE so it opens right away

$CURRENT_DIR = Get-Location

Set-Location $PROJECT_DIR

if (-Not (Test-Path $WORKSPACE_FILE)) {
    Write-Error "VS Code workspace file '${WORKSPACE_FILE}' doesn't exist!"
    exit 1
}

# Launch VS Code for Beancount with user-data and extensions-dir set to
# isolation paths
& $VSCODE `
    --verbose `
    --user-data-dir ".\.vscode\user-data\" `
    --extensions-dir ".\.vscode\extensions\" `
    $WORKSPACE_FILE `
    $args

# Return the user's shell back to where they started
Set-Location ${CURRENT_DIR}
