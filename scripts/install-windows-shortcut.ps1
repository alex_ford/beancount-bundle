<#
    Windows PowerShell script that "installs" the Windows Shortcut for launching
    Beancount (VS Code). After installation, try pressing the [WIN] key, then
    typing "Beancount (VS Code)"!

    Author: Alex Richard Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License
#>

# Run Anywhere Block - this allows the script to be launched from any working dir.
$SCRIPTS_DIR = "${PSScriptRoot}"
$PROJECT_DIR = Split-Path -Parent "${SCRIPTS_DIR}"
$WORKSPACE_FILE = "vscode-beancount.code-workspace"

# Toggle one of these depending on if you'd like to see debug output
$DebugPreference = "SilentlyContinue"
# $DebugPreference = "Continue"

# Permanently define %USER_LOCAL_BIN% environment variable for current user
if (!$(Test-Path -Path "${env:LOCALAPPDATA}\bin")) {
    Write-Output "Creating $env:LOCALAPPDATA\bin directory..."
    New-Item -ItemType Directory -Path "$env:LOCALAPPDATA\bin"
}
Set-Variable -Name "USER_LOCAL_BIN" -Value $(Resolve-Path "${env:LOCALAPPDATA}\bin").Path
Write-Output "`$USER_LOCAL_BIN => `"${USER_LOCAL_BIN}`""

# TODO: Permanently modify path to include %USER_LOCAL_BIN%
$PATH = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
Write-Output "`$PATH => `"${PATH}`""

if (${PATH}.Contains(${USER_LOCAL_BIN})) {
    Write-Output "`"${USER_LOCAL_BIN}`" is already on the user's path."
}
else {
    Write-Output "Adding `"${USER_LOCAL_BIN}`" to the user's path since it isn't there."
    $PATH = "${PATH};${USER_LOCAL_BIN}"
    Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value "${PATH}"
}

$PATH = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
Write-Output "`$PATH => `"${PATH}`""

# $PATH = "C:\Program Files (x86)\Common Files\Oracle\Java\javapath;C:\Python38\Scripts\;C:\Python38\;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\ProgramData\chocolatey\bin;C:\Program Files\Git\cmd;C:\Program Files\Microsoft VS Code\bin;C:\HashiCorp\Vagrant\bin;C:\Program Files\dotnet\;C:\Program Files\FileBot\;C:\Program Files (x86)\GNU\GnuPG\pub;C:\Program Files\OpenJDK\openjdk-11.0.7_10\bin;"

# Copy/link Beancount (VS Code) shortcut into %USER_LOCAL_BIN%
# TODO: see if this should be replaced with what ARFWiki does - generates the shortcut link?
# Copy-Item -Verbose -Path "${SCRIPTS_DIR}\Beancount (VS Code).lnk" -Destination "${USER_LOCAL_BIN}"

# Create a Shortcut with Windows PowerShell
Write-Output "Creating shortcut (.lnk) file..."
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut("${USER_LOCAL_BIN}\Beancount (VS Code).lnk")
$Shortcut.TargetPath = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
$Shortcut.Arguments = "${SCRIPTS_DIR}\vscode-beancount-remote-ssh.ps1"
# $Shortcut.IconLocation = "${ARFWIKI_APPS_DIR}\arfwiki-icon-01.ico"
Write-Output "Saving shortcut file to '${USER_LOCAL_BIN}' directory..."
$Shortcut.Save()
Write-Debug ${Shortcut}

Write-Output "Installation of shortcut has finished!"
