#!/bin/bash

# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see './LICENSE')

# Grab various paths for use by this script, supporting ubiqutous execution from any location
SCRIPT_PATH=$(realpath $0)
SCRIPTS_DIR=$(dirname ${SCRIPT_PATH})
BEANCOUNT_BUNDLE_DIR=$(dirname ${SCRIPTS_DIR})
BEANCOUNT_ROOT_DIR=$(dirname ${BEANCOUNT_BUNDLE_DIR})

# Save the current working directory so we can return later
CURRENT_DIR=$(pwd)

# Change to the 'beancount' root directory where all the other beancount-* projects will be cloned
cd ${BEANCOUNT_ROOT_DIR}

# The rest of these are the Git sub-projects as they are on their 'master' branches
# TODO: should these actually default to the https versions so that they can be cloned by folks without GitLab accounts?
# Could maintain an option that uses the git@gitlab.com URLs for development.
# TODO: replace the git clone lines with a list & loop implementation for better code quality
git clone git@gitlab.com:alex_ford/beancount-arfsrc.git
git clone git@gitlab.com:alex_ford/beancount-cli.git
git clone git@gitlab.com:alex_ford/beancount-fava-src.git
git clone git@gitlab.com:alex_ford/beancount-fava-docker.git
git clone git@gitlab.com:alex_ford/beancount-ins.git
git clone git@gitlab.com:alex_ford/beancount-ionic.git
git clone git@gitlab.com:alex_ford/beancount-scheduler.git
git clone git@gitlab.com:alex_ford/beancount-scripts.git    # private?
git clone git@gitlab.com:alex_ford/beancount-secure.git
git clone git@gitlab.com:alex_ford/beancount-templates.git

# TODO: detect if a data repo already exists - if yes, don't do most of the following
# TODO: ask/provide option if user wants a shallow copy or full copy of the data repo
# TODO: add logging/output functions with colorized level tag

echo "[ INFO ] You now need a PRIVATE Git repo for storage of your beancount data."
echo "[ INFO ] enter the Git URL to an existing repo now, if you have one, otherwise simply press enter."
read -p "URL: " bcdUrl

if [ "${bcdUrl}" != "" ]; then
    mkdir -p ~/Data/Personal/
    echo "[ INFO ] cloning ${bcdUrl}..."
    git -C ~/Data/Personal/ clone "${bcdUrl}"
else
    echo "[ INFO ] no URL specified, skipping data repo cloning."
    #echo "Will create a templated data directory for you now!"
    #echo "COMING SOON"
    # TODO: use a template/scaffolding directory as the starting point for new beancount data repos
fi

# Return the terminal back to where we came from
cd ${CURRENT_DIR}

echo "[ INFO ] $0 has finished."

