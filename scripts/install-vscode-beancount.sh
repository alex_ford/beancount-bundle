#!/bin/bash

###
# Installs a "bin link" so that the following:
#     `vscode-beancount`
#     `bean-vscode`
# can be executed from a command line like any other Linux command.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
# Website: http://www.alexrichardford.com
# License: MIT License (see './LICENSE')
###

echo "[ INFO ] Installing VSCode Beancount..."

# Determine if we need to use `sudo` or not!
which sudo >/dev/null 2>&1
if [ $? == 0 ]; then
  USE_SUDO="sudo"
else
  USE_SUDO=""
fi

# Determine user's local bin directory
USER_BIN_DIR="$(realpath ${HOME}/.local/bin/)"
if [ ! -e "${BIN_DIR}" ]; then
  #USER_BIN_DIR="$(dirname $(which code))"
  USER_BIN_DIR="/usr/local/bin/"
  if [ ! -e "${USER_BIN_DIR}" ]; then
    echo "[ ERRO ] USER_BIN_DIR (${USER_BIN_DIR}) does not exist!"
    exit 1
  fi
fi

# Run-anywhere setup!
THIS_SCRIPT="$(realpath ${0})"
BUNDLE_SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
BUNDLE_HOME_DIR="$(dirname ${BUNDLE_SCRIPTS_DIR})"

echo "[ INFO ] installing 'bin links' to: ${USER_BIN_DIR}"

function make_bin_link() {
  BIN_LINK_NAME="${1}"
  if [ "${BIN_LINK_NAME}" == "" ]; then echo "[ ERRO ] missing function param BIN_LINK_NAME!"; exit 1; fi
  EXEC_PATH="$(realpath ${2})"
  if [ "${EXEC_PATH}" == "" ]; then echo "[ ERRO ] missing function param EXEC_PATH!"; exit 1; fi
  if [ ! -e "${EXEC_PATH}" ]; then echo "[ ERRO ] EXEC_PATH `${EXEC_PATH}` does not exist!"; exit 1; fi

  # Derived params
  BIN_LINK_PATH="${USER_BIN_DIR}/${BIN_LINK_NAME}"

  if [ -e "${BIN_LINK_PATH}" ]; then
    echo "[ WARN ] bin link '${BIN_LINK_PATH}' already exists! It will be replaced!"
    ${USE_SUDO} unlink "${BIN_LINK_PATH}"
  fi

  ${USE_SUDO} ln -s "${EXEC_PATH}" "${BIN_LINK_PATH}"
}

make_bin_link "vscode-beancount" ./vscode-beancount.sh
make_bin_link "bean-vscode" ./vscode-beancount.sh

echo "[ INFO ] Install finished! Restart Bash, or re-source your .bashrc file."
