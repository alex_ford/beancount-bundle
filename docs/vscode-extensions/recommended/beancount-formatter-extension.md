Name: Beancount Formatter
Id: dongfg.vscode-beancount-formatter
Description: Beancount file formatter
Version: 1.2.0
Publisher: dongfg
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=dongfg.vscode-beancount-formatter
