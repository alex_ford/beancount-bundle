# VS Code Extensions

This folder contains documentation on various Visual Studio Code **Extensions** that are recommended, or have been tried, for working with Beancount data files.

## Recommended Extensions

- [Beancount <small><tt>lencerf.beancount</tt></small>](./recommended/beancount-extension.md)
- [Beancount Formatter <small><tt>dongfg.vscode-beancount-formatter</tt></small>](./recommended/beancount-formatter-extension.md)
- [Python <small><tt>ms-python.python</tt></small>](./recommended/microsoft-python-extension.md)
- [Markdown All in One <small><tt>yzhang.markdown-all-in-one</tt></small>](./recommended/markdown-all-in-one.md)
- [Commands <small><tt>fabiospampinato.vscode-commands</tt></small>](./recommended/commands-extension.md)
- [{NAME} <small><tt>{ID}</tt></small>](./recommended/{PAGE-SLUG}.md)

## Tried Extensions

These extensions have been tried, but for one reason or another, are not recommended for the specific use case of a Beancount VS Code IDE.

- ...
