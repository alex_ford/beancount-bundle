# Accessing Online APIs

Several utilities in the various ARF Beancount Projects interact with different Online APIs in order to accomplish their function. Some of those APIs require some amount of configuration, such as specifying an API key. Generally, this should all be done within the `./beancount-data/configuration/apis/` directory.

See documentation for the specific utility for what configuration should actually be specified.

Example: say we have a utility that uses the AbcDef API (fictituous, of course!). This API requires the use of a Private Key. After we obtain this Private Key from the API's website, we place it in the `./beancount-data/configuration/apis/AbcDef.key` file as the only content.
