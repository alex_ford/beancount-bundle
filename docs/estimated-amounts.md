# Estimated Amounts

An **estimated amount** is a value which is not exactly known, but there is a good estimate of what it will be. Enter these amounts in a normal transaction, but use the _posting flag_ syntax to denote that the amount is estimated.

```beancount
2019-06-22 ! "Payee"   "Narr"
  ! Liabilities:Credit-Cards:My-Credit-Card      -20.00 USD
  Expenses:Shopping
```

By definition, a Transaction Directive with a posting whose amount is estimated should also be marked as pending at the Transaction level.

