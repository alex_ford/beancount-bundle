# Sharing Expenses Routinely

See also [Sharing Expenses Intermittantly](./sharing-expenses-intermittantly.md).

-   Use [SettleUp](https://app.settleup.io) to share the tracking of split expenses. Use this for common/daily transactions and other small-ish amounts.
-   Certain large amounts, like Rent, will be handled separately.
-   A split based on salary ratio seems fair:
    -   85000 / 145000 = 0.586207

# List of split/shared bills

-   Verizon cell phone
-   Electricity
-   Gas
-   Rent
