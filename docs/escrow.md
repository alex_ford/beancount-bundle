# Escrow

Escrow is used when funds move between accounts, but may not necessarily post to both accounts at approximately the same time.

-   Use a pair of transaction directives to represent the movement of funds.
-   Use the `Equity:Escrow` account as an intermediary between the two accounts.
-   Use a link to help associate the transactions together.

Example:

```beancount
2019-04-16 * "TRANSFER"   "From A to B"   ^link123
  Assets:A      -100.00 USD
  Equity:Escrow  100.00 USD

2019-04-18 * "TRANSFER"   "From A to B"   ^link123
  Equity:Escrow  -100.00 USD
  Assets:B        100.00 USD
```
