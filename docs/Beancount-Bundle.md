# Beancount Bundle

This is the meta-project for my collection of Beancount mini-projects. This exists to help make it easier to use and organize things. It also serves as the generic documentation space, covering Beancount topics that aren't specific or isolated to one of the mini-projects.

## Key Reference Info

-   [Official Beancount Docs on Google Docs](<>)
-   [Sharing Expenses with Erik](./sharing-expenses-with-erik.md)

## Best Practices Docs and General Guidelines

-   CSV files should be used as the preferred input/import format.
-   Try to use the output from Mint! If this works well, it will cover a bunch of accounts all at once.
-   Git should be used for Beancount data repositories.
-   Commit and Push, when using Git, as often as possible.
    -   Use a tool (such as my `git-commit-sync` tool) to help!

## HOWTO Docs

### Exchanging Currencies

Unless you are doing so for investment purposes, you generally want to exchange currencies **without cost specification**.

#### Without Cost Specification

```beancount
2018-11-09 * "GDAX" "Bought Bitcoin"
  time: "08:28 EST"
  exchange-rate: "6357.03 USD/BTC"
  Assets:Investments:GDAX:USD                                    -100.00 USD
  Expenses:Financial:Commisions                                     0.30 USD
  Equity:Trade-Conversion                                          99.70 USD
  Equity:Trade-Conversion	                                         -0.01568356 BTC
  Assets:Investments:GDAX:BTC                                       0.01568356 BTC
```

#### With Cost Specification

```beancount
TBD
```
