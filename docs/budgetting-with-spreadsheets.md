# Budgetting with Spreadsheets

This doc describes how to use spreadsheet software (Google Sheets recommended) to support budgetting with Beancount.

## Exporting Income and Expense Accounts

1. Use the core `bean-report` tool to extract your Income and Expense accounts.
    - Use the `beancount-scripts` tool `collect_all_accounts`:
        ```terminal
        pipenv run python .\beancount_scripts\collect_all_accounts.py --inex-only --sort ASC
        ```
    - Linux
        ```bash
        bean-report ./main.beancount accounts | grep -e "Income|Expenses"
        ```
    - Windows
        ```powershell
        bean-report .\main.beancount accounts | Select-String -Pattern "Income|Expenses"
        ```
2. Open your desired spreadsheet software (Google Sheets is recommended!) and type "Account" into cell A1.
3. Copy the output and paste into the spreadsheet starting at A2.
4. Style and arrange as you wish, but include a "Frequency" and "Amount" column.
5. Frequency should be specified in (days in a year) / (period)!
    1. So, a bi-weekly paycheck would be: (365) / (14) = 26.071428571428573
