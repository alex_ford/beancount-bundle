# VSCode Beancount Find (idea)

This idea is for giving VS Code's built-in search/find capabilities an enhanced understanding of Beancount data structure and format. Providing search/find prefixes to give the user a psuedo-query language to help most easily find what is being looked for.

e.g. press CTRL + F to search the current file, then enter `payee:Ben` which will then return results only for transactions which have "Ben" in the payee field.
