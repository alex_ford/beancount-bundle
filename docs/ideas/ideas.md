# Ideas

This section contains documentation on various ideas which haven't been explored or implemented yet in VS Code for Beancount.

- [Bitcoin Price Hover](./bitcoin-price-hover.md)

- [Transaction Finisher](./tx-finisher.md)

- Date Picker
    - A small date picker widget (something like [this](https://jqueryui.com/datepicker/)) can be presented to the user when:
        - They request it from the Command Palette.
        - They press CTRL+Space while the cursor is within a YYYY-MM-DD formatted text.

- Date Calculations

- Transaction Flag Toggler
    - Easily toggle the transaction flag by hovering over the transaction to see a CodeLens menu with the option.
