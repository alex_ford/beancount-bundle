# Tx Finisher

Transaction Finisher would be a VS Code Command that when activated by the user would attempt to "finish" a transaction.

-   While Beancount allows for 1 posting in a transaction to not specify a quantity, it's more clear for the user when they are specified... Tx Finisher will automatically compute the blank posting.
