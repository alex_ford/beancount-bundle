# Bitcoin Price Hover

In the VS Code Editor, how feasible would it be to detect if/when the user is hovering over a numerical quantity that is immediately followed by a space and then the characters `BTC`, and on detection a popup would show with the current Bitcoin price, and the converted value to USD?
