# `bean-query` notes

- Shows the future transactions for accounts under the `Assets:Banking` hierarchy. 
```
select * where account ~ "Assets:Banking" and date > today()
```

- Shows all transactions with the specified flag symbol:
```
select * where flag = "*"
select * where flag = "!"
```

- Shows transactions from `Assets:Banking:Ally:Checking` for the month of March 2020:
```
beancount> select * where account = "Assets:Banking:Ally:Checking" and date >= date(2020,03,01) and date < date(2020,04,01)
```
