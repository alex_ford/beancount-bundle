# Encrypting Data at Rest

## Git-Crypt

-   (-) difficult to view the history of beancount files, as the history is stored encrypted.
-   (-)(-) automatic Git merging doesn't seem to work!!! **DANGER**
-   (+) simple enough to use
-   (+) options to use either GPG signatures or externally managed keyfiles

## Veracrypt

Coming soon...
