# Beancount Integrated Development Environment (IDE)

The term "Beancount IDE" is used in this documentation to refer to this entire project's purpose: using Microsoft's open source, and free, [Visual Studio Code](https://code.visualstudio.com) (VS Code) as a one-stop shop for working with Beancount!

## Why VS Code?

## Who is this for?

## Leveraged Built-in Features

VS Code is a great starting point for Beancount users thanks to it's very extensible design, as well as due to many of the base/core features built right into the editor:

- ...
- [Markdown](https://code.visualstudio.com/docs/languages/markdown)
    - The built-in Markdown Previewer is a great light-weight viewer for Markdown files.
- Git
    - ...
- [Python](https://code.visualstudio.com/docs/languages/python)
    - [Getting Started with Python in VS Code](https://code.visualstudio.com/docs/python/python-tutorial)
