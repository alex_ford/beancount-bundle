# Getting Started

1.  Create/update virtual environment:
    ```shell
    cd ../beancount/beancount-bundle
    git pull
    pipenv install
    ```
2.  Use the startup script to bootstrap and launch the python launcher for VS Code:
    ```shell
    ./start-vscode-beancount.ps1 (or .sh)
    ```
