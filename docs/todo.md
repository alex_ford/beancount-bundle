# Todo List

From within VS Code, use the line movement hotkey (default: <kbd>ALT</kbd>+<kbd>Up/Down</kbd>) to organize todos into the different sections of this markdown file.

- [Todo List](#todo-list)
  - [Critical](#critical)
  - [High](#high)
  - [Medium](#medium)
  - [Low](#low)
  - [Deprioritized](#deprioritized)

## Critical

## High

-   [ ] TODO: use Beancount logo for Windows shortcut that launches VSCode Beancount

## Medium

-   [ ] TODO: scheduled transaction processor

## Low

## Deprioritized
