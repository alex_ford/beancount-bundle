#!/usr/env python3

"""
Launcher script for Beancount Bundle using Microsoft Visual Studio Code!

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License (see LICENSE file)
"""

import colorlog
import os
import sys
import subprocess
import shutil
import arrow
import argparse

_log = colorlog.getLogger(__name__)

class VSCodeEnvironment():

    vscode_exe = None
    git_exe = None

    # TODO: set these in a better way/place
    project_dir = "./"
    code_workspace_file = None
    extensions_dir = "./.vscode/extensions/"
    user_data_dir = "./.vscode/user-data/"

    def __init__(self, workspace_file_name: str):
        self.code_workspace_file = "/".join([self.project_dir, 
            ".".join([workspace_file_name, "code-workspace"])])
        self.vscode_exe = shutil.which("code")
        self.git_exe = shutil.which("git")

    # TODO: have this accept an array of file strings
    def open(self, files: list()):
        """Mimics calling the 'code' binary directory from the terminal, but
        includes automatically setting the extensions and user-data directories
        to support an isolated vscode environment."""
        _log.info("Launching Visual Studio Code!")
        vscode_args = [
            self.vscode_exe,
            "--verbose",
            "--user-data-dir", self.user_data_dir,
            "--extensions-dir", self.extensions_dir,
            self.code_workspace_file,
            # self.CURRENT_MONTH_TX_FILE,
            # self.LAST_MONTH_TX_FILE,
            # self.DOCS_STARTING_FILE
        ]
        vscode_args.extend(files)
        vscode_exe_return = subprocess.run(
            vscode_args,
            check=True
        )
        _log.debug(f"'vscode_exe_return': {vscode_exe_return}")


def main():
    """Open Visual Studio Code in an isolated, dedicated configuration."""
    _logHandler = colorlog.StreamHandler()
    _logHandler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(_logHandler)
    _log.setLevel("INFO")
    _log.info("VS Code Beancount is starting!")

    parser = argparse.ArgumentParser(
        description="Launches or opens the named files in an isolated, "
                    "dedicated instance of Visual Studio Code separated from "
                    "your system's instance."
    )
    parser.add_argument("files",
        nargs="*",
        help="Optional list of files to open. If VSCode is already open, it "
             "will be re-used to open the additional files named."
    )
    args = parser.parse_args()

    vscode = VSCodeEnvironment("vscode-beancount")
    vscode.open(args.files)

if __name__ == "__main__":
    main()
