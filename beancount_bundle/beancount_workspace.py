class BeancountWorkspace():
    """Entry-point for managing a complex Beancount Visual Studio Code workspace."""

    def check_var(self, varName: str, varValue: str):
        """
        Simple function that prints out the name and value of the specified arg,
        as well as checks that it references a path on the filesystem which exists.
        Raises an AssertionError if the path does not exist, or the argument is
        undefined in the environment vars.

        Arguments:
            name {str} -- [description]
        """
        #value = eval(name)
        _log.info(f"{varName} is: {varValue}")
        assert varValue != None, f"{varName} cannot be NoneType!"
        assert os.path.exists(varValue), f"{varValue} does not exist!"


    def check_var_create(self, varName: str, varValue: str):
        """Check the named variable as a path, creating it if it doesn't exist."""
        # TODO: figure out how to merge these two methods together
        try:
            self.check_var(varName, varValue)
        except AssertionError as ae:
            if ae.__str__().__contains__("does not exist"):
                _log.info(f"Will create {varValue} now.")
                # TODO: create it, throw error on fail
                os.makedirs(varValue)
                assert os.path.exists(varValue), f"A problem occurred while trying to create '{varValue}'!"
                _log.info(f"'{varValue}' created succesfully!")
            else:
                raise ae

    def resolve_environment(self):
        self.VSCODE = shutil.which("code")
        self.check_var("VSCODE", self.VSCODE)

        self.GIT = shutil.which("git")
        self.check_var("GIT", self.GIT)

        self.BUNDLE_SRC_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))
        self.check_var("BUNDLE_SRC_DIR", self.BUNDLE_SRC_DIR)

        self.BUNDLE_HOME_DIR = os.path.dirname(self.BUNDLE_SRC_DIR)
        self.check_var("BUNDLE_HOME_DIR", self.BUNDLE_HOME_DIR)

        self.BEANCOUNT_BASE_DIR = os.path.dirname(self.BUNDLE_HOME_DIR)
        self.check_var("BEANCOUNT_BASE_DIR", self.BEANCOUNT_BASE_DIR)

        self.BEANCOUNT_DATA_DIR = os.path.join(self.BEANCOUNT_BASE_DIR, "beancount-data")
        self.check_var("BEANCOUNT_DATA_DIR", self.BEANCOUNT_DATA_DIR)

        self.WORKSPACE_FILE = os.path.join(self.BUNDLE_HOME_DIR, "beancount.code-workspace")
        self.check_var("WORKSPACE_FILE", self.WORKSPACE_FILE)

        self.VSCODE_USER_DATA_DIR = os.path.join(self.BUNDLE_HOME_DIR, ".vscode/user-data/")
        self.check_var_create("VSCODE_USER_DATA_DIR", self.VSCODE_USER_DATA_DIR)

        self.VSCODE_EXTENSIONS_DIR = os.path.join(self.BUNDLE_HOME_DIR, ".vscode/extensions/")
        self.check_var_create("VSCODE_EXTENSIONS_DIR", self.VSCODE_EXTENSIONS_DIR)

        self.CURRENT_MONTH = arrow.now()
        self.LAST_MONTH = self.CURRENT_MONTH.shift(months=-1)

        self.LAST_MONTH_DATA_FILE = "{}/{}{:02d}.beancount".format(
                                                        self.LAST_MONTH.year,
                                                        self.LAST_MONTH.year,
                                                        self.LAST_MONTH.month)

        self.CURRENT_MONTH_DATA_FILE = "{}/{}{:02d}.beancount".format(
                                                        self.CURRENT_MONTH.year,
                                                        self.CURRENT_MONTH.year,
                                                        self.CURRENT_MONTH.month)

        self.LAST_MONTH_TX_FILE = os.path.realpath("{}/transactions/{}".format(
                                                        self.BEANCOUNT_DATA_DIR,
                                                        self.LAST_MONTH_DATA_FILE))
        self.check_var("LAST_MONTH_TX_FILE", self.LAST_MONTH_TX_FILE)

        # TODO: change the rest of these above to use f-strings
        #CURRENT_MONTH_TX_FILE = os.path.realpath("{}/transactions/{}".format(
        #                                                       BEANCOUNT_DATA_DIR,
        #                                                       CURRENT_MONTH_DATA_FILE))
        self.CURRENT_MONTH_TX_FILE = os.path.realpath(f"{self.BEANCOUNT_DATA_DIR}/transactions/{self.CURRENT_MONTH_DATA_FILE}")
        self.check_var("CURRENT_MONTH_TX_FILE", self.CURRENT_MONTH_TX_FILE)

        # TODO: dynamically resolve next month's file, maybe if we're like 15 days away from the next month?

        self.ACCOUNTS_FILE = os.path.realpath(f"{self.BEANCOUNT_DATA_DIR}/configuration/accounts.beancount")
        self.check_var("ACCOUNTS_FILE", self.ACCOUNTS_FILE)

        self.DOCS_STARTING_FILE = os.path.join(self.BUNDLE_HOME_DIR, "docs/Beancount-Bundle.md")
        self.check_var("DOCS_STARTING_FILE", self.DOCS_STARTING_FILE)


    def install_or_update_virtualenv(self):
        """
        This basically just calls the pipenv commands to install and/or update the
        virtual environment. Essentially same as:
            ```shell
            pipenv install
            pipenv update
            ```
        """
        pass


    def sync_git_repos(self):
        """
        Provides a simple function for updating the contents of a local Git repo.
        """
        # TODO: this is currently trying to perform git ops on files and non-git directories! bad!
        # DISABLED UNTIL FIXED
        return
        _log.info("Synchronizing Git repo data for all 'beancount' repos...")
        beancount_dirs = os.listdir(self.BEANCOUNT_BASE_DIR)
        # TODO: this can take awhile, so only do the bundle and data dirs as
            # blocking, then do the rest of the dirs in the background.
        errors = list()
        for d in beancount_dirs:
            path = "{}/{}".format(self.BEANCOUNT_BASE_DIR, d)
            _log.info(path)
            os.chdir(path)
            try:
                completion = subprocess.run([
                    GIT,
                    "pull"
                ], check=True)
            except subprocess.CalledProcessError as e:
                _log.error("Caught exception! {}".format(e))
                errors.append(e)
            _log.info(completion)
        if len(errors) > 0:
            _log.error("{} errors where detected during the Git pull operation!".format(len(errors)))
            for e in errors:
                _log.error(e)


def main():
    beancountWksp = BeancountWorkspace()
    beancountWksp.resolve_environment()
    # TODO: consider installing autopep8/pylint/etc.
    # TODO: consider scanning extensions and performing the install right here,
        # instead of having the user manually do it
    # TODO: add --check argument that performs all checks but does not do
        # anything else (no git ops, no launching vscode)

    beancountWksp.sync_git_repos()

    _log.info("Launching Visual Studio Code!")
    completion = subprocess.run([
            beancountWksp.VSCODE,
            "--verbose",
            "--user-data-dir", beancountWksp.VSCODE_USER_DATA_DIR,
            "--extensions-dir", beancountWksp.VSCODE_EXTENSIONS_DIR,
            beancountWksp.WORKSPACE_FILE,
            beancountWksp.CURRENT_MONTH_TX_FILE,
            beancountWksp.LAST_MONTH_TX_FILE,
            beancountWksp.DOCS_STARTING_FILE
        ],
        check=True
    )
    _log.debug(f"Completion is: {completion}")

if __name__ == "__main__":
    _logHandler = colorlog.StreamHandler()
    _logHandler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(_logHandler)
    _log.setLevel("INFO")
    _log.info("Beancount VS Code Bundle is starting!")
    main()
    # TODO: skip-getting-started and disable-telemetry args
